[//]: # (** DO NOT EDIT this file directly! **  It is auto-generated.  Changes should be made to financial_institutions.sql or gen_fi_table.sh instead.)

# Directory of US-based brokerages
## Whitelist

The following brokerages have no significant ethical issues:

| *brokerage* | *ALEC member* | *Tor-hostile* | *sensitive info exposed to CloudFlare* | *supported CISPA* | *forced drug testing of staff* | *notes* |
|---|---|---|---|---|---|---|
|[Greenvest](https://greenvest.com)|n|n|n|n|n|RIRA; min. investment to avoid fees: $100k; [B corp](https://bcorporation.net/directory/greenvest)|
|[Rich Uncles](https://richuncles.com)|n|n|n|n|n|Real estate investing only; verification requires giving them a phone number that you answer (voicemail is not accepted)|

## Graylist

These brokerages would normally be blacklisted, but due to the short whitelist they are set aside as a less evil compromise to those blacklisted. They should still be avoided if possible.

| *brokerage* | *ALEC member* | *Tor-hostile* | *sensitive info exposed to CloudFlare* | *supported CISPA* | *forced drug testing of staff* | *notes* |
|---|---|---|---|---|---|---|
|[E*Trade](https://us.etrade.com)|n|n|n|🕵|n|covers: Canada, France, Germany, Hong Kong, Japan, United Kingdom, and United States; funding bonus ($5k=>$50; $10k=>$100; $20k=>$150)|
|[Finhabits](https://www.finhabits.com)|n|n|n|n|n|[B corp](https://bcorporation.net/directory/finhabits); Hosted on Google Cloud; outsources to Apex; uses Equifax for address verification; sends spam|
|[nvstr](https://www.nvstr.com)|n|n|n|n|n|**Amazon AWS-hosted**; maintenance fee: $4/month; promos: $15-150 for funding, random bonus awards, referral bonuses|
|[Robinhood](https://robinhood.com/us/en/)|n|n|n|n|n|**Amazon AWS-hosted**; [Fined $65M](https://nypost.com/2020/12/17/sec-slaps-robinhood-app-with-65m-fine-for-misleading-users) for misleading users; They may have used CloudFlare in the past but apparently that changed; robotic support only -- human support is [difficult or impossible](https://mastodon.social/@cypnk/105802268148593250) to reach; all trades are on a margin and transfers to another broker are marked as such; transfers can take up to a month so any trades like options may expire or lose value since you cannot initiate without knowing a timeline|
|[Stash](https://stash.com)|n|n|n|n|n|**Amazon AWS-hosted**; no mutual funds; no options; no crypto; maintenance fee: $1/month|
|[tastyworks](https://tastyworks.com)|n|n|n|n|n|**Amazon AWS-hosted**; TIRA; RIRA; no forex; no crypto; no non-US stocks; open/close fee= $0/0; commission=$5/stock trade (closing trades gratis), $1/option trade; promo: funding (100 shares [$1-6ea, avg:$200-220]), referral=$75|
|[TradeStation](https://www.tradestation.com)|n|n|n|n|n|**Amazon AWS-hosted**; crypto; min. invest=$500 ($2k for bonus); open/close fee= $0/0; commission=$0.50/option trade; commission=$0-5/stock trade|
|[Webull](https://www.webull.com)|n|n|n|n|n|**Amazon AWS-hosted**; TIRA; RIRA; crypto; no forex|

## Blacklist

These brokerages have severe ethical or trust issues and should be boycotted:

| *brokerage* | *ALEC member* | *Tor-hostile* | *sensitive info exposed to CloudFlare* | *supported CISPA* | *forced drug testing of staff* | *notes* |
|---|---|---|---|---|---|---|
|Ally|n|👁|n|🕵|n|whole site is Tor-hostile (403 error)|
|Axos Invest|n|n|🌩|n|n|**Amazon AWS-hosted**; Axos Bank is jailed in CloudFlare and Axos Invest will be soon. Investors who get blocked by that will have to pay $5/month for paper statements.|
|Betterment|n|n|n|n|n|**forced h/reCAPTCHA**; Imposes hCAPTCHA just to read the landing page. They censor posts in their Reddit sub that complains about this instead of offering support.|
|Charles Schwab|n|👁|n|🕵|🧪|akamai-hosted; will report your interest earnings to the IRS via 1099-int even if the earnings for the year are below the reporting threshold of $10; majority owner of TD Ameritrade; outsources banking operations to PNC Bank; [caught paying](https://www.rollingstone.com/politics/politics-features/dark-money-republican-party-americans-for-job-security-peter-thiel-devos-904900) $8.8 million to republican dark money group "Americans for Job Security", which has [ties](https://www.issueone.org/dark-money-groups-american-for-job-security) to the Koch brothers and neglected to register as a political committee in order to conceal the money trail; [donated](https://web.archive.org/web/20210126185141/publicintegrity.org/politics/donald-trump-inauguration-bankrolled-by-corporate-giants) $1 million to Trump's inaugeration just before Charles Schwab's granddaughter [got a whitehouse gig](https://www.huffpost.com/entry/white-house-samantha-schwab_n_59137723e4b021221db9d490) in an apparent *pay-to-play* scheme. [Called out by Lincoln Project](https://twitter.com/ProjectLincoln/status/1349405850906087424) for financing Trump; [maxed out donations](https://archive.thinkprogress.org/trump-rnc-legal-fund-de045351554c) to Trump's legal fund to the tune of $6.7 million as a way to hide contributions that escape the transparency of campaign contributions; Also got caught in a dark money conspiracy in California using the Koch brothers to hide political contributions to republicans to the tune of $11 million.|
|Ellevest|n|n|🌩|n|n|**Amazon AWS-hosted**|
|Euro Pacific Capital|n|n|🌩|n|n||
|Fidelity|👌|👁|n|🕵|🧪|sponsors Fox News; akamai hosted; takes voiceprints of customers without express consent; Landing page is Tor-friendly but transactional site is Tor-hostile|
|Firstrade|n|👁|n|n|n|whole site is Tor-hostile (468 error)|
|Fundrise|n|n|n|n|n|**forced h/reCAPTCHA**; **Amazon AWS-hosted**|
|FUTU|n|n|n|n|n|support.fututrade.com is CloudFlared; no web app; mobile app is GPS-iOS-only; desktop is Mac/Windows only|
|Gatsby|n|n|n|n|n|no web app; no desktop app; mobile app is GPS-iOS-only|
|IEX|n|👁|n|n|n|an alternative to conventional stock markets; **Google-Cloud hosted**|
|InteractiveBrokers|n|👁|n|n|n|min. investment to avoid fees: $100k per account; has an "impact" feature to analyze the portfolio's ESG factors w.r.t. the user's ethical views; covers Australia, Belgium, Canada, France, Germany, Hong Kong, Italy, Japan, Mexico, Netherlands, Singapore, South Korea, Spain, Sweden, Switzerland, United Kingdom, and United States; website is **partially Tor-hostile** but the website can be avoided for most post-registration activities like trading and fetching statements|
|Janus Henderson|n|👁|n|n|n|**Amazon AWS-hosted**; transactional web host (www.secureaccountview.com) is not AWS, but it is Tor-hostile; working offline and receiving gratis paper statements is possible.|
|Lightspeed|n|n|🌩|n|n||
|M1 Finance|n|n|🌩|n|n|They censor posts in their Reddit sub that expose the risks of passing sensitive financial data through CloudFlare.|
|Merrill Edge|n|n|n|n|🧪|Owned by one of the [most evil][banklist] banks in the world (Bank of America)|
|Prudential|👌|n|n|🕵|🧪|**Amazon AWS-hosted**|
|Siebert|n|n|🌩|n|n||
|SoFi|n|n|🌩|n|n|They censor posts in their Reddit sub that expose the risks of passing sensitive financial data through CloudFlare; also [caught](https://www.ftc.gov/news-events/press-releases/2018/10/online-student-loan-refinance-company-sofi-settles-ftc-charges) in a deceptive advertizing scandal.|
|Sogotrade|n|n|🌩|n|n||
|Stockpile|n|n|🌩|n|n||
|TD Ameritrade (TDA)|n|n|n|n|n|Majority owned by Charles Schwab; Schwab outsources banking operations to PNC bank (an [evil bank][banklist]); [uses MS Github](https://github.com/TDAmeritrade/stumpy) to host s/w; TDA has had several [data breaches](https://www.zdnet.com/article/report-td-ameritrade-data-leak-started-in-2005), one occurance of which leaked email addresses [impacting 6.2 million customers](https://web.archive.org/web/20130501215431/www.computerworld.com/s/article/9037083/TD_Ameritrade_was_warned_of_possible_data_breach_months_ago) and led to ransom demands and pump 'n' dump stock scams; Scottrade also had a [data breach in 2015](https://web.archive.org/web/20201123223450/https://fortune.com/2015/10/02/scottrade-data-breach), 2 years before TDA acquired it|
|TIAA-CREF|n|👁|n|n|n|Whole site is Tor-hostile; uses Ally Bank for banking; uses Pershing LLC (a subsidiary of BNY Mellon Corp) for clearing; BNY Mellon was [breached in 2008](https://web.archive.org/web/20160308134258/www.wctv.tv/news/headlines/28132494.html)|
|Tradingblock|n|n|🌩|n|n||
|Vanguard|n|👁|n|n|🧪|[Citation](https://gitlab.torproject.org/legacy/trac/-/wikis/org/doc/ListOfServicesBlockingTor#banking-finance) for Tor blocking.|
|Wealthfront|n|n|n|n|n|**forced h/reCAPTCHA**; Registration imposes Google reCAPTCHA; [caught](https://www.jpost.com/Breaking-News/US-regulator-sanctions-robo-advisers-Wealthfront-Hedgeable-on-false-disclosures-575044) making false disclosures and [charged](https://www.eastbaytimes.com/2018/12/21/bay-area-robo-adviser-firm-wealthfront-charged-by-sec-with-false-advertising) for false advertising.|
|Wealthsimple|n|n|🌩|n|n||
|Wellstrade|n|n|n|🕵|🧪|Owned by Wells Fargo, an [evil][banklist] bank.|
|Zackstrade|n|n|🌩|n|n||

[banklist]: <usa_banks.md>
[RAP-amazon]: <rap_sheets/amazon.md>
[RAP-cf]: <rap_sheets/cloudflare.md>
[RAP-pp]: <rap_sheets/paypal.md>

# Why ALEC members are blacklisted

American Legislative Exchange Council ("ALEC") is a right-wing super
PAC and bill mill that prioritizes corporate interests above the
interest of human beings.  Specifically, ALEC:

* [fights environmental protections](https://www.alecexposed.org/wiki/Environment,_Energy,_and_Agriculture)
* [fights gun control](https://www.alecexposed.org/wiki/Guns,_Prisons,_Crime,_and_Immigration)
* [fights healthcare](https://www.alecexposed.org/wiki/Health,_Pharmaceuticals,_and_Safety_Net_Programs)
* [fights immigration](https://www.alecexposed.org/wiki/Guns,_Prisons,_Crime,_and_Immigration)
* [fights worker's rights](https://www.alecexposed.org/wiki/Worker_Rights_and_Consumer_Rights)
* [fights consumer protections](https://www.alecexposed.org/wiki/Worker_Rights_and_Consumer_Rights)
* [fights public education](https://www.alecexposed.org/wiki/Privatizing_Public_Education,_Higher_Ed_Policy,_and_Teachers)
* fights women's rights
* fights voter rights ([supports voter suppression policy](https://www.alecexposed.org/wiki/Democracy,_Voter_Rights,_and_Federal_Power))
* [finances republicans](https://www.sourcewatch.org/index.php?title=ALEC_Civil_Justice_Task_Force#Politicians)
* supports the NRA

Countless companies were ALEC members historically, but most of them discontinued membership and renounced it likely to avoid boycott.
Companies that continue to renew their ALEC membership are right-wing die-hards unlikely to join team humanity. So they are blacklisted.

The OK hand sign (👌) indicates that the financial institution still today supports the above-mentioned right-wing agenda through ALEC membership.

## Why Tor-hostile FIs are blacklisted

Financial institutions that are part of the blockade against innocent Tor-users are automatically blacklisted.

<details>
<summary>Why access to banks, brokerages, and insurance companies over Tor matters</summary>
If Tor were used exclusively for anonymity, it would be useless in the
context of consumers accessing and controlling their financial
accounts.  But that's not the case.  Tor prevents your ISP from
snooping on where you bank.  ISPs collect data on their own customers
and exploit it for profit in the US.  Under Obama it became illegal
for an ISP to sell data collected on their customers without express
consent. As if that's not already useless thanks to an abundant supply
of consumers who will agree to anything without reading it, Trump
<a href="https://www.nbcnews.com/news/us-news/trump-signs-measure-let-isps-sell-your-data-without-consent-n742316">reversed</a>
Obama's policy in 2017 to render consumers completely powerless.  Tor
is a free tool to protect from excessive disclosure of where your
assets are.  Thus when a financial institution blocks Tor, it prevents you
from taking basic self-defense measures.  This trend undermines the
supplier-client relationship whereby we expect the supplier to serve
the customer's interest.  It's not just anti-privacy, it's
anti-consumer.

Non-Tor users generally reveal their physical location to their bank
or insurance company every time they login.  If all banks and
insurance companies didn't care where you reside, this wouldn't be a
problem.  But some financial institutions care more than others and
beyond reason.  Banks typically [collect your IP address](
https://web.archive.org/web/20201024203113/www.decorahbank.com/legal-information/privacy-policy)
and one bank even outright admits in their [privacy policy](
https://web.archive.org/web/20210206141004/beneficialstatebank.com/uploads/files/BSB-Consumer-Privacy-Act-CCPA-Privacy-Notice-Current-6.4.2020.pdf#page=2)
that they collect geolocation data from customers' IP addresses. For
nomads/world travelers banks can make their lives hell if their
profile doesn't seem to match up with their lifestyle.  Some banks
will close an account if a customer moves out of their service area.
It's worth noting the fine print at the bottom of the [Simmons Bank
website](https://www.simmonsbank.com), which states "This site is
directed at, and intended to be used by, persons in the United States
of America only." As another demonstration, try accessing the [Marcus
website](https://www.marcus.com) from outside the US; it will reveal
that Marcus denies their clients access to their accounts even if they
simply leave the US for a vacation. Insurance companies will question
whether you're still eligible for the policy you have, as they may
want to raise your premiums or cancel your policy if they suspect
you're not where your policy is written.  If you want to take a job
away from home for a year or two, Tor gives you the necessary privacy
to do that free of hassle and nannying.  </details>

<details>
<summary>Why non-Tor users should also boycott Tor adversaries</summary>
Suppose you never leave home, and you're not bothered if your ISP
collects data on where you bank to then sell to data brokers who can
then sell it to debt collectors.  If you're ethical nonetheless, then
you still boycott those who marginalize Tor users.  These quotes
elaborate on that moral duty:

"*If you are neutral in situations of injustice, you have chosen the
side of the oppressor. If an elephant has its foot on the tail of a
mouse, and you say that you are neutral, the mouse will not appreciate
your neutrality.*" --Desmond Tutu

"*Arguing that you don't care about the right to privacy because you
have nothing to hide is no different than saying you don't care about
free speech because you have nothing to say.*" --Edward Snowden

To expand on Snowden's philosophy, it's extremely selfish to refuse to
defend a right that others need on the basis that you don't personally
need it now or in the future.  Moreover, indirect benefits should not
be overlooked.  Human rights activists need civil liberties more than
others, but we all need activists to make the world better for
everyone.  Moral duties to you derive from that.

Tor is becoming less usable because the growing majority non-Tor users
are patronizing businesses that marginalize Tor users.

"*Under observation, we act less free, which means we effectively are
less free.*" --Edward Snowden

To neglect to use Tor is to subject yourself to unnecessary
observation.  In the context of banking and finance, this in turn
reduces your freedom of movement.
</details>

<details><summary>Special case: Homesite Insurance Group</summary>
An exception to blacklisting is given to Homesite Insurance Group (aka
Midvale Home & Auto) because only the quoting page blocks Tor users
and it's separate from all other resources.  Since you can get quotes
over the phone we relaxed the blacklisting in their case.  Consumers
are of course free to make their own choice anyway.
</details>

<details><summary>Special case: InteractiveBrokers</summary>
InteractiveBrokers's (IB) trading platform supports proxies over Tor
which makes it possible to use Tor for trading.  It's also possible to
receive electronic statements and paper tax documents without using
the website.  So the Tor-blocking website is not an obstical to most
routine operations.  Nonetheless, it's a considerable problem that
initial registration and configuration can't be done over Tor.  And
you may need to login to the website after registration to modify data
subscriptions, initiate a funds transfer, or read messages.  We don't
have a dark gray list, so we ultimately blacklisted IB.  Consumers are
of course free to make their own choice anyway.
</details>

The eye (👁) indicates that account access is restricted and exclusive to non-Tor users,
who must expose their IP address to the FI and who must expose their FI to their ISP.

## Why FIs in CloudFlare's walled-garden are blacklisted

Financial institutions that proxy their services through CloudFlare
are blacklisted automatically for taking a profoundly stupid risk with
consumer's sensitive financial data.  CloudFlare holds the SSL keys
for every connection and sees all the traffic including username and
unhashed password.  CloudFlare has proven to be untrustworthy with
sensitive information (demonstrated by CloudFlare's doxxing of the
identities of child porn whistle blowers).  Apart from the
unacceptably high security risk of having a CloudFlare MitM, there are
countless [ethical problems][RAP-cf] with being an enabler of
CloudFlare.

The storm cloud (🌩) indicates that account access is restricted and
exclusive per CloudFlare's will and customers who do get access are
forced to share sensitive transaction data with CloudFlare, Inc. (a
privacy abuser).

## Why some FIs that force CAPTCHAs are blacklisted

Banks and brokerages that force customers to solve an hCAPTCHA or a
Google reCAPTCHA are blacklisted automatically. Use of these two forms
of CAPTCHA have an excessive detrimental impact on privacy and human
rights, which is outlined in the [CloudFlare rap sheet][RAP-cf].

Other forms of CAPTCHA aren't so invasive and aren't cause for
blacklisting.

## Why FIs that impose Google Playstore (GPS) or Apple are blacklisted 

Banks and brokerages that force customers to obtain software from
Google Playstore or Apple are blacklisted automatically. Most brokers
have web access or a desktop app, in which case the mobile app can be
disregarded because customers have a viable means to avoid the privacy
abusing walled gardens.

FIs like FUTU and Gatsby are a problem. Gatsby has no means of access
apart from the mobile app, and no APK is available on their website or
in f-droid.org, so Android users have no choice but to buy mobile
phone service, trust Google with their phone number, then also trust
Google not to tell data brokers where you bank and invest. FUTU has a
desktop app but only for Mac or Windows, so linux users and those who
avoid non-free software are stuffed. (Caveat: the FUTU Windows app has
[not been tested on WINE](
https://web.archive.org/web/20210206141122/https://www.winehq.org/search?q=futu)
or ReactOS)

Google Playstore is
[scientifically proven](https://nsl.cs.waseda.ac.jp/wp-content/uploads/2018/04/submitted_wama2017.pdf)
to be relatively insecure compared to F-Droid in the "*Understanding
the Security Management of Global Third-Party Android Marketplaces*"
article.  Also noteworthy is
[F-Droid: The privacy-friendly alternative to Google Play Store](https://android.izzysoft.de/articles/named/fdroid-intro-1).
Another [study](https://core.ac.uk/download/pdf/142058929.pdf#page=64)
found financial applications on Android to have a propensity to call
for over-priviledged permissions and to call the protected android
methods excessively.

## Why CISPA supporters are graylisted

The [Cyber Intelligence Sharing and Protection Act (CISPA)](
https://en.wikipedia.org/wiki/Cyber_Intelligence_Sharing_and_Protection_Act)
was a bill to bypass the 4th amendment to promote a system of
unwarranted mass surveillance through information sharing between the
government and private sector.  Congress blocked the bill, but it was
later reincarnated as CISA and it passed.  Unlike ALEC lobbying, CISPA
was a one-off event far in the past, and over 800 companies supported
it.  Since it does not necessarily reflect the company's recent stance
or influence, supporters are graylisted instead of blacklisted.  They
should still be avoided in favor of a whitelisted competitor, but they
are considerably less evil than those that are blacklisted.

The spook (🕵) indicates that the financial institution lobbied for a
police surveillance state in favor of CISPA.

## Why FIs that force their staff to take a drug test are graylisted

Drug testing employees is an assault on the privacy and lifestyle of
employees and staff outside the workplace.  In most cases involving
medicinal marijuana states, the drug test also harms the healthcare of
employees by intervening in doctors' prescriptions.  Normally drug
testing would justify blacklisting, but the problem is so widespread
nationwide that the whitelist tends to be overly small.  Drug testing
also does not do significant harm to consumers, so companies that drug
test are graylisted.

The test tube (🧪) indicates that the financial institution abuses
their staff through forced drug testing.

## Why Amazon and Google-hosted FIs are graylisted

Amazon is behind [countless evils][RAP-amazon].  It's paramount to
boycott Amazon for anyone who cares about human rights, privacy, or
the environment.  Amazon also has had several data breaches-- Capital
One, Juspay, Swiggy, etc., so it's a bad idea to trust custodians who
use AWS with the security of your money.  Google had to ditch their
"don't be evil" slogan, but Amazon is still a greater evil.  Both
Google and Amazon are in the fossil fuel business.  Google is also a
tech giant which (like Amazon) serving as a central point of
surveillance and also carries the risks of having a huge number of
insiders who can abuse the data.  The size of the Amazon and Google
datacenters also makes them a likely target for outside hackers due to
the high rewards of compromise.

Akamai is not known to have a significant history of wrongdoing on the
scale of Amazon or Google.  There is cause for concern in terms of
security though because it's large enough to serve as a central
monitoring point where breaches and compromise is still considerable.
Akamai-hosted financial institutions are not graylisted for that
reason alone.  In the end, you're the judge.

Financial institutions hosted on GAFAM (Google Amazon Facebook Apple
Microsoft) are graylisted.  The wrongdoing is indirect and in the end
taking a security risk doesn't necessarily lead to a breach.  Of
course it's still ethically favorable to choose a whitelisted
financial institution if possible.

## Why FIs that outsource to Equifax are graylisted

Equifax is the most reckless of all four credit bureaus with sensitive
credit data. A data breach of sensitive consumer records to the tune
of ~150 million Americans was leaked as a result of reckless security
procedures. The lawsuit yielded a disproportionately tiny settlement
by which most Americans were not compensated, even though they never
consented to Equifax collecting the data to begin with. Of those who
received compensation, most did not receive cash, but rather credit
protection service which ultimately feeds more money back into the
credit bureaus.

## An FI is only as good as its supply chain and ownership

Financial institutions like Merrill Edge, TD Ameritrade, and
Wellstrade have ethically controversial ownership.  Merrill Edge and
Wellstrade are simply owned by extremely [unethical banks][banklist].
A large majority of TD Ameritrade is owned by Charles Schwab.  Schwab
is not directly involved in the highly controversial financing that
other large banks are, but Schwab outsources banking to PNC bank,
which is an ALEC member with total disregard for humanity.

It's important for ethical consumption to consider the whole supply
chain to the extent of your awareness.  When consuming a product or
service you're not just feeding the immediate customer-facing
business.

We track both the supply chain and ownership.  We will not give a
subsidiary a higher rating than its parent.
