# liberethos_paradigm

Our mission: supply information and tools to facilitate ethical consumption of goods and services.

The following catalogs include blacklists, whitelists, and graylists of financial institutions:

* [banks in the US](usa_banks.md)
* [brokerages in the US](usa_brokerages.md)
* [insurance companies in the US](usa_insurance_companies.md)

