create table if not exists fiTbl (name        text primary key not null,
                                  url         text,
                                  parent      text,
                                  fi_kind     text check(fi_kind    in ('bank', 'brokerage', 'CU', 'payment processor', 'insurer')) not null default 'brokerage',
                                  lst_kind    text check(lst_kind   in ('black', 'gray', 'white'))                                  not null default 'white',
                                  hrecaptcha  text check(hrecaptcha in ('unavoidable', 'non-essential tasks', 'never'))             not null default 'never',
                                  cflogin     boolean not null default 0,
                                  antitor     boolean not null default 0,
                                  alec        boolean not null default 0,
                                  forced_nfsw boolean not null default 0,
                                  foxnews     boolean not null default 0,
                                  aws         boolean not null default 0,
                                  cispa       boolean not null default 0,
                                  dt          boolean not null default 0,
                                  notes       text);
/* meanings:

   fiTbl <= table of financial institutions
   parent <= name of parent company
   fi_kind <= kind of financial institution
   hrecaptcha <= If the FI pushes an hCAPTCHA or Google reCAPTCHA, we track whether it's forced on customers in all situations,
                 or if it's only used for things like submission of a non-essential form. We don't care about other kinds of CAPTCHAs.
   cflogin <= If the transactional (login) host for a service uses CloudFlare (tor-hostile or not), this boolean is true.
              If only non-transactional pages use CF then this boolean is false, but we note it in the notes.
   antitor <= If a non-CF site blocks or impedes Tor users (often by 403 error) then this boolean is true.
              This is still false for Tor-hostile CF blockades because we capture that with cflogin & hrecaptcha attributes.
   alec <= True if the FI is a /current/ ALEC member.  We're not vindictive toward former members.
   forced_nfsw <= Forced use of non-free software.  All FIs distribute non-free software, but we set this to true if there is no means of access with free software.
   foxnews <= True if the FI sponsored Fox News, which promotes right-wing extremism.
   aws <= True if the FI's transactional site is hosted by Amazon AWS, thus promoting an evil corp while exposing sensitive data to it.
   cispa <= True if the FI lobbied in favor of the CISPA act.
   dt <= True if the FI forces staff to take drug tests.
*/

/* BEGIN BROKERAGES */

/* insert into fiTbl (name,notes) values ("Evertrade","Where's the website? Sold to TIAA-CREF?");*/

insert into fiTbl (name,url,antitor,cispa,notes) values ('Ally','http://www.ally.com/',1,1,'whole site is Tor-hostile (403 error)');

insert into fiTbl (name,url,cflogin,aws,notes) values ('Axos Invest','https://www.axosinvest.com',1,1,
   'Axos Bank is jailed in CloudFlare and Axos Invest will be soon. Investors who get blocked by that will have to pay $5/month for paper statements.');
  
insert into fiTbl (name,url,hrecaptcha,notes) values ('Betterment','https://www.betterment.com','unavoidable',
   'Imposes hCAPTCHA just to read the landing page. They censor posts in their Reddit sub that complains about this instead of offering support.');

insert into fiTbl (name,url,antitor,cispa,dt,notes) values ('Charles Schwab','https://www.schwab.com',1,1,1,
   'akamai-hosted; will report your interest earnings to the IRS via 1099-int even if the earnings for the year are below the reporting threshold of $10; majority owner of TD Ameritrade; outsources banking operations to PNC Bank;'||
   ' [caught paying](https://www.rollingstone.com/politics/politics-features/dark-money-republican-party-americans-for-job-security-peter-thiel-devos-904900)'||
   ' $8.8 million to republican dark money group "Americans for Job Security", which has [ties](https://www.issueone.org/dark-money-groups-american-for-job-security)'||
   ' to the Koch brothers and neglected to register as a political committee in order to conceal the money trail;'||
   ' [donated](https://web.archive.org/web/20210126185141/publicintegrity.org/politics/donald-trump-inauguration-bankrolled-by-corporate-giants) $1 million'||
   ' to Trump''s inaugeration just before Charles Schwab''s granddaughter'||
   ' [got a whitehouse gig](https://www.huffpost.com/entry/white-house-samantha-schwab_n_59137723e4b021221db9d490)'||
   ' in an apparent *pay-to-play* scheme. [Called out by Lincoln Project](https://twitter.com/ProjectLincoln/status/1349405850906087424) for financing Trump;'||
   ' [maxed out donations](https://archive.thinkprogress.org/trump-rnc-legal-fund-de045351554c) to Trump''s legal fund to the tune of $6.7 million'||
   ' as a way to hide contributions that escape the transparency of campaign contributions;'||
   ' Also got caught in a dark money conspiracy in California using the Koch brothers to hide political contributions to republicans to the tune of $11 million.');

insert into fiTbl (name,url,cispa,notes) values ('E*Trade','https://us.etrade.com',1,
   'covers: Canada, France, Germany, Hong Kong, Japan, United Kingdom, and United States; funding bonus ($5k=>$50; $10k=>$100; $20k=>$150)');

insert into fiTbl (name,url,cflogin,aws) values ('Ellevest','https://www.ellevest.com',1,1);
insert into fiTbl (name,url,cflogin)     values ('Euro Pacific Capital','https://europacbank.com',1);

insert into fiTbl (name,url,forced_nfsw,notes) values ('FUTU','fututrade.com',1,
   'support.fututrade.com is CloudFlared; no web app; mobile app is GPS-iOS-only; desktop is Mac/Windows only');

insert into fiTbl (name,url,alec,antitor,foxnews,cispa,dt,notes) values
  ('Fidelity','https://www.fidelity.com',1,1,1,1,1,
   'akamai hosted; takes voiceprints of customers without express consent; Landing page is Tor-friendly but transactional site is Tor-hostile');

insert into fiTbl (name,url,notes) values ('Finhabits','https://www.finhabits.com',
   '[B corp](https://bcorporation.net/directory/finhabits); Hosted on Google Cloud; outsources to Apex; uses Equifax for address verification; sends spam');

insert into fiTbl (name,url,antitor,notes)     values ('Firstrade','https://firstrade.com',1,'whole site is Tor-hostile (468 error)');
insert into fiTbl (name,url,hrecaptcha,aws)    values ('Fundrise','https://fundrise.com','unavoidable',1);
insert into fiTbl (name,url,forced_nfsw,notes) values ('Gatsby','trygatsby.com',1,'no web app; no desktop app; mobile app is GPS-iOS-only');

insert into fiTbl (name,url,notes) values ('Greenvest','https://greenvest.com','RIRA; min. investment to avoid fees: $100k; [B corp](https://bcorporation.net/directory/greenvest)');

insert into fiTbl (name,url,antitor,notes) values ('IEX','https://iextrading.com/trading',1,'an alternative to conventional stock markets; **Google-Cloud hosted**');

insert into fiTbl (name,url,antitor,notes) values ('InteractiveBrokers','https://interactivebrokers.com',1,
   'min. investment to avoid fees: $100k per account; has an "impact" feature to analyze the portfolio''s ESG factors w.r.t. the user''s ethical views;'||
   ' covers Australia, Belgium, Canada, France, Germany, Hong Kong, Italy, Japan, Mexico, Netherlands, Singapore, South Korea, Spain, Sweden, Switzerland,'||
   ' United Kingdom, and United States; website is **partially Tor-hostile** but the website can be avoided for most post-registration activities like trading and'||
   ' fetching statements');

insert into fiTbl (name,url,antitor,aws,notes) values ('Janus Henderson','https://www.janushenderson.com',1,1,
   'transactional web host (www.secureaccountview.com) is not AWS, but it is Tor-hostile; working offline and receiving gratis paper statements is possible.');

insert into fiTbl (name,url,cflogin) values ('Lightspeed','https://www.lightspeed.com',1);

insert into fiTbl (name,url,cflogin,notes) values ('M1 Finance','https://www.m1finance.com',1,
   'They censor posts in their Reddit sub that expose the risks of passing sensitive financial data through CloudFlare.');

insert into fiTbl (name,url,parent,dt,notes) values ('Merrill Edge','https://www.merrilledge.com/','Bank of America',1,
   'Owned by one of the [most evil][banklist] banks in the world (Bank of America)');

insert into fiTbl (name,url,aws,notes) values ('nvstr','https://www.nvstr.com',1,
   'maintenance fee: $4/month; promos: $15-150 for funding, random bonus awards, referral bonuses');

insert into fiTbl (name,url,alec,aws,cispa,dt) values ('Prudential','https://www.prudential.com',1,1,1,1);

insert into fiTbl (name,url,notes) values ('Rich Uncles','https://richuncles.com',
   'Real estate investing only; verification requires giving them a phone number that you answer (voicemail is not accepted)');

insert into fiTbl (name,url,aws,notes) values ('Robinhood','https://robinhood.com/us/en/',1,
   '[Fined $65M](https://nypost.com/2020/12/17/sec-slaps-robinhood-app-with-65m-fine-for-misleading-users) for misleading users;'||
   ' They may have used CloudFlare in the past but apparently that changed;'||
   ' robotic support only -- human support is [difficult or impossible](https://mastodon.social/@cypnk/105802268148593250) to reach;'||
   ' all trades are on a margin and transfers to another broker are marked as such;'||
   ' transfers can take up to a month so any trades like options may expire or lose value since you cannot initiate without knowing a timeline');

insert into fiTbl (name,url,cflogin) values ('Siebert','https://www.siebert.com',1);

insert into fiTbl (name,url,cflogin,notes) values ('SoFi','https://sofi.com/',1,
   'They censor posts in their Reddit sub that expose the risks of passing sensitive financial data through CloudFlare;'||
   ' also [caught](https://www.ftc.gov/news-events/press-releases/2018/10/online-student-loan-refinance-company-sofi-settles-ftc-charges)'||
   ' in a deceptive advertizing scandal.');

insert into fiTbl (name,url,cflogin)   values ('Sogotrade','https://sogotrade.com',1);
insert into fiTbl (name,url,aws,notes) values ('Stash','https://stash.com',1, 'no mutual funds; no options; no crypto; maintenance fee: $1/month');
insert into fiTbl (name,url,cflogin)   values ('Stockpile','https://www.stockpile.com',1);

insert into fiTbl (name,url,aws,notes) values ('tastyworks','https://tastyworks.com',1,
   'TIRA; RIRA; no forex; no crypto; no non-US stocks; open/close fee= $0/0; commission=$5/stock trade (closing trades gratis), $1/option trade;'||
   ' promo: funding (100 shares [$1-6ea, avg:$200-220]), referral=$75');

/* Schwab does not wholly own TDA, but we say schwab is a parent of TDA in the db to ensure inheritance of the black list status */
insert into fiTbl (name,url,parent,notes) values ('TD Ameritrade (TDA)','https://www.tdameritrade.com','Charles Schwab',
   'Majority owned by Charles Schwab; Schwab outsources banking operations to PNC bank (an [evil bank][banklist]);'||
   ' [uses MS Github](https://github.com/TDAmeritrade/stumpy) to host s/w; TDA has had several '||
   '[data breaches](https://www.zdnet.com/article/report-td-ameritrade-data-leak-started-in-2005), one occurance of which leaked email addresses'||
   ' [impacting 6.2 million customers](https://web.archive.org/web/20130501215431/www.computerworld.com/s/article/9037083/TD_Ameritrade_was_warned_of_possible_data_breach_months_ago)'||
   ' and led to ransom demands and pump ''n'' dump stock scams; Scottrade also had a '||
   '[data breach in 2015](https://web.archive.org/web/20201123223450/https://fortune.com/2015/10/02/scottrade-data-breach), 2 years before TDA acquired it');

insert into fiTbl (name,url,antitor,notes) values ('TIAA-CREF','https://tiaa-cref.org',1,
   'Whole site is Tor-hostile; uses Ally Bank for banking; uses Pershing LLC (a subsidiary of BNY Mellon Corp) for clearing;'||
   ' BNY Mellon was [breached in 2008](https://web.archive.org/web/20160308134258/www.wctv.tv/news/headlines/28132494.html)');

insert into fiTbl (name,url,aws,notes) values ('TradeStation','https://www.tradestation.com',1,
   'crypto; min. invest=$500 ($2k for bonus); open/close fee= $0/0; commission=$0.50/option trade; commission=$0-5/stock trade');

insert into fiTbl (name,url,cflogin) values ('Tradingblock','https://tradingblock.com',1);

insert into fiTbl (name,url,antitor,dt,notes) values ('Vanguard','https://investor.vanguard.com',1,1,
   '[Citation](https://gitlab.torproject.org/legacy/trac/-/wikis/org/doc/ListOfServicesBlockingTor#banking-finance) for Tor blocking.');

insert into fiTbl (name,url,hrecaptcha,notes) values ('Wealthfront','https://www.wealthfront.com','unavoidable',
   'Registration imposes Google reCAPTCHA;'||
   ' [caught](https://www.jpost.com/Breaking-News/US-regulator-sanctions-robo-advisers-Wealthfront-Hedgeable-on-false-disclosures-575044)'||
   ' making false disclosures and [charged](https://www.eastbaytimes.com/2018/12/21/bay-area-robo-adviser-firm-wealthfront-charged-by-sec-with-false-advertising)'||
   ' for false advertising.');

insert into fiTbl (name,url,cflogin)               values ('Wealthsimple','https://wealthsimple.com',1);
insert into fiTbl (name,url,aws,notes)             values ('Webull','https://www.webull.com',1,'TIRA; RIRA; crypto; no forex');
insert into fiTbl (name,url,parent,cispa,dt,notes) values ('Wellstrade','https://wellstrade','Wells Fargo',1,1,'Owned by Wells Fargo, an [evil][banklist] bank.');
insert into fiTbl (name,url,cflogin)               values ('Zackstrade','https://zackstrade.com',1);

/* END BROKERAGES */
/* BEGIN INSURERS */

insert into fiTbl (fi_kind,name,url,parent)                              values ('insurer','21st Century','https://www.21st.com','Farmers');
insert into fiTbl (fi_kind,name,url,alec,foxnews,dt,notes)               values ('insurer','Aflac','https://www.aflac.com',1,1,1,'transactional site is **Google Cloud-hosted**');
insert into fiTbl (fi_kind,name,url,cispa,dt,aws)                        values ('insurer','Allianz','https://allianz.com',1,1,1);
insert into fiTbl (fi_kind,name,url,parent,cispa,dt,aws,antitor)         values ('insurer','Allied','https://www.alliedinsurance.com','Nationwide',1,1,1,1);
insert into fiTbl (fi_kind,name,url,cispa,dt,antitor,foxnews,notes)      values ('insurer','Allstate','https://www.allstate.com',1,1,1,1,
  'akamai hosted; [accused](https://www.consumerreports.org/car-insurance/allstate-car-insurance-pricing-michigan-regulators-raise-objections)'||
  ' by Michigan regulators of profiling customers unlikely to shop out insurance to charge them more, and accused in Texas of having a "suckers list";'||
  ' [uses "personalized pricing" in 10 states](https://www.consumerreports.org/car-insurance/why-you-may-be-paying-too-much-for-your-car-insurance).');
insert into fiTbl (fi_kind,name,url)                                     values ('insurer','American Family Insurance','https://www.amfam.com');
insert into fiTbl (fi_kind,name,url,hrecaptcha,cflogin,notes)            values ('insurer','American Modern','https://www.amig.com','unavoidable',1,
  '**Google Cloud-hosted** landing page, which is CloudFlare-free but the transactional host my.doculivery.com is CFd');
insert into fiTbl (fi_kind,name,url,cispa,dt,notes)                      values ('insurer','Ameriprise Financial','https://www.ameriprise.com',1,1,'akamai hosted');
insert into fiTbl (fi_kind,name,url,antitor)                             values ('insurer','Amica','https://www.amica.com',1);
insert into fiTbl (fi_kind,name,parent,notes)                            values ('insurer','American Strategic Insurance (ASI)','Progressive',
  'no website, only an access-restricted MS LinkedIn page');
insert into fiTbl (fi_kind,name,url,dt,notes)                            values ('insurer','Berkshire Hathaway','https://berkshirehathaway.com',1,
  'Berkshire Hathaway is not directly an ALEC member, but BH wholly owns ALEC members (e.g. Geico and Fruit of the Loom)');
insert into fiTbl (fi_kind,name,url,hrecaptcha,cflogin,cispa)            values ('insurer','Brown & Brown Insurance','https://bbinsurance.com','unavoidable',1,1);
insert into fiTbl (fi_kind,name,url,notes)                               values ('insurer','CUNA Mutual','https://www.cunamutual.com','Feeds LMG through TruStage.');
insert into fiTbl (fi_kind,name,url)                                     values ('insurer','Erie','https://www.erieinsurance.com');
insert into fiTbl (fi_kind,name,url,parent,foxnews,notes)                values ('insurer','Esurance','https://www.esurance.com','Allstate',1,'akamai hosted');
insert into fiTbl (fi_kind,name,url,alec,dt,notes)                       values ('insurer','Farmers','https://www.farmers.com',1,1,'akamai hosted; [caught](https://web.archive.org/web/20210102154321/https://publicintegrity.org/politics/republican-lawmakers-posh-hideaway-bankrolled-by-secret-corporate-cash) financing the Cloakroom project.');
insert into fiTbl (fi_kind,name,url,parent,dt,notes)                     values ('insurer','First American Insurance Agency','https://www.faiagency.com','Liberty Mutual',1,"[data breach in 2019](https://gizmodo.com/885-million-sensitive-records-leaked-online-bank-trans-1835016235) (warning: Gizmodo link has popups-- better source needed)");
insert into fiTbl (fi_kind,name,url,parent,dt)                           values ('insurer','Foremost','http://www.foremost.com','Farmers',1);
insert into fiTbl (fi_kind,name,url,parent,alec,foxnews,dt,notes)        values ('insurer','Geico','https://www.geico.com','Berkshire Hathaway',1,1,1,
  'akamai hosted but transactional site ecams.geico.com is not.');
insert into fiTbl (fi_kind,name,url,parent,notes)                        values ('insurer','Gen Re','https://www.genre.com','Berkshire Hathaway',
  'akamai hosted, but transactional site www.genre-connect.com is not. The documentation ("Knowledge" link) is CloudFlared.');
insert into fiTbl (fi_kind,name,url,dt,notes)                            values ('insurer','National General','https://www.nationalgeneral.com',1,'formerly GMAC');
insert into fiTbl (fi_kind,name,url,dt)                                  values ('insurer','Grange Mutual','https://www.grangeinsurance.com',1);
insert into fiTbl (fi_kind,name,url,parent)                              values ('insurer','Harleysville Group','https://www.harleysvillegroup.com','Nationwide');
insert into fiTbl (fi_kind,name,url,antitor,cispa,dt,notes)              values ('insurer','Hartford','https://www.thehartford.com',1,1,1,'akamai hosted');
insert into fiTbl (fi_kind,name,url,notes)                               values ('insurer','Homesite Insurance Group (aka Midvale Home & Auto)','https://go.midvaleinsurance.com',
  'affiliated with American Family Insurance; landing page is Fastly-hosted; quoting page is AWS-hosted & Tor-hostile but it''s non-essential; login page has no issues');
insert into fiTbl (fi_kind,name,dt,notes)                                values ('insurer','Horace Mann',1,'no website, only an access-restricted Facebook page');
insert into fiTbl (fi_kind,name,url,antitor,dt)                          values ('insurer','Infinity','https://infinityauto.com',1,1);
insert into fiTbl (fi_kind,name,url,antitor,alec,notes)                  values ('insurer','Lexington','https://www.lexingtoninsurance.com',1,1,
  'landing page allows Tor access but all links therein refuse Tor; AIG partner');
insert into fiTbl (fi_kind,name,url,antitor,alec,foxnews,cispa,notes)    values ('insurer','Liberty Mutual','https://www.libertymutual.com',1,1,1,1,'akamai hosted');
insert into fiTbl (fi_kind,name,url,parent,antitor,notes)                values ('insurer','Main Street America Insurance','https://msainsurance.com','American Family Insurance',1,
  'Landing page allows Tor but the transactional host does not');
insert into fiTbl (fi_kind,name,url,antitor,dt)                          values ('insurer','Mercury','https://www.mercuryinsurance.com',1,1);
insert into fiTbl (fi_kind,name,url,cispa,dt,aws,notes)                  values ('insurer','MetLife','https://www.metlife.com',1,1,1,
  'transactional site identity.metlife.com is not AWS');
insert into fiTbl (fi_kind,name,notes)                                   values ('insurer','N&D Group','no website, only an access-restricted Facebook page');
insert into fiTbl (fi_kind,name,url,antitor,aws,alec,foxnews,cispa,dt)   values ('insurer','Nationwide','https://nationwide.com',1,1,1,1,1,1);
insert into fiTbl (fi_kind,name,url,aws,dt,notes)                        values ('insurer','New Jersey Manufacturers (NJM)','https://www.njm.com',1,1,'despite the name they are not limited to New Jersey');
insert into fiTbl (fi_kind,name,url,antitor,dt)                          values ('insurer','Pemco','https://pemco.com',1,1);
insert into fiTbl (fi_kind,name,url,antitor,foxnews,dt)                  values ('insurer','Progressive','https://progressive.com',1,1,1);
insert into fiTbl (fi_kind,name,url,antitor,notes)                       values ('insurer','Safe Auto','http://www.safeauto.com',1,
  '**Tor-hostile** sign-in page despite Tor-friendly landing page.');
insert into fiTbl (fi_kind,name,url,parent,antitor,notes)                values ('insurer','Safeco','https://safeco.com','Liberty Mutual',1,'akamai hosted');
insert into fiTbl (fi_kind,name,url,dt,notes)                            values ('insurer','Selective','https://www.selective.com',1,
  'pushes CloudFlare javascript, but apparently execution is optional.');
insert into fiTbl (fi_kind,name,url,dt,notes)                            values ('insurer','Shelter Insurance','https://web.archive.org/web/shelterinsurance.com',1,
  'CloudFlare name server is used, which means they can trivially flip a switch to become a CF site.');
insert into fiTbl (fi_kind,name,url,antitor,alec,foxnews,cispa,dt,notes) values ('insurer','State Farm','https://www.statefarm.com',1,1,1,1,1,'edgecast-hosted');
insert into fiTbl (fi_kind,name,url,aws,dt)                              values ('insurer','Stewart Information Services Corporation','https://www.stewart.com/en.html',1,1);
insert into fiTbl (fi_kind,name,url,parent,notes)                        values ('insurer','The General','https://thegeneral.com','American Family Insurance',
  'only writes auto policies; certified as [Great Place to Work](https://www.greatplacetowork.com/certified-company/7003720)');
insert into fiTbl (fi_kind,name,url,parent)                              values ('insurer','Titan','https://titaninsured.com','Nationwide');
insert into fiTbl (fi_kind,name,url,hrecaptcha,cispa,dt,notes)           values ('insurer','Travelers','https://www.travelers.com','unavoidable',1,1,'akamai hosted');
insert into fiTbl (fi_kind,name,url,parent,notes)                        values ('insurer','TruStage','https://www.trustage.com','CUNA Mutual',
  'home and auto policies underwritten by Liberty Mutual (LMG)');
insert into fiTbl (fi_kind,name,url,antitor,foxnews,cispa,dt)            values ('insurer','USAA','https://www.usaa.com',1,1,1,1);
insert into fiTbl (fi_kind,name,url,hrecaptcha,cflogin)                  values ('insurer','Western Mutual','https://www.westernmutual.com','unavoidable',1);

/* END INSURERS */

update fiTbl set lst_kind = 'gray'
             where lst_kind = 'white' and (aws or cispa or dt or (notes is not null and (notes like '%equifax%' or notes like '%google_cloud_hosted%')));
update fiTbl set lst_kind = 'black'
             where cflogin or alec or antitor or forced_nfsw or hrecaptcha = 'unavoidable' or parent in ('Bank of America','Wells Fargo') or notes like '%underwritten_by%LMG%';
update fiTbl set lst_kind = 'black'
             where name like '%CUNA_Mutual%' or parent in ('Bank of America','Wells Fargo') or notes like '%underwritten_by%LMG%'; /* hacks */
update fiTbl set lst_kind = 'black' where parent in (select name from fiTbl where lst_kind = 'black');
update fiTbl set lst_kind = 'gray'  where parent in (select name from fiTbl where lst_kind = 'gray') and lst_kind = 'white';
update fiTbl set notes = 'parent: '||parent||case when notes is null then '' else '; '||notes end
             where parent is not null and (notes is null or notes not like '%'||parent||'%');
update fiTbl set notes = '**Amazon AWS-hosted**'||case when notes is null then '' else '; '||notes end where aws;
update fiTbl set notes = 'sponsors Fox News'||case when notes is null then '' else '; '||notes end where foxnews;
update fiTbl set notes = '**forced h/reCAPTCHA**'||case when notes is null then '' else '; '||notes end where hrecaptcha = 'unavoidable';
/* update fiTbl set notes = '**Tor-hostile** resources'||case when notes is null then '' else '; '||notes end where antitor and (notes is null or notes not like '%tor_hostile%');*/
