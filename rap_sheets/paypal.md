1. Paypal is a **privacy** abuser:
    1. PayPal shares customers' data with [600 companies](https://www.schneier.com/blog/archives/2018/03/the_600_compani.html).
    1. PayPal goes overboard on the KYC, blocking accounts on KYC grounds when enough info is known to legally service an account.  This is an needless assault on privacy. *(citation needed)*
    1. PayPal no longer allows people to use it without creating an account.  If you try to use it without creating an account, an account is created for you automatically. *(citation needed)*
    1. Accounts cannot be deleted.  Accounts may only be disabled/closed *manually* [by phone request](https://github.com/justdeleteme/justdelete.me/pull/481).  Deletion is impossible, as PayPal retains all the data indefinitely.  *(citation needed- it would likely violate the GDPR to refuse deletion to a European)*
    1. Paypal has a nannying policy scrutinize customers transacting for the purpose of:
       * providing file sharing services (VPNs are treated as such despite not being itemized in their ban notice, but it's in their policy)
       * access to newsgroups
       * selling alcoholic beverages
       * non-cigarette tobacco products or e-cigarettes
       * prescription drugs/devices
       * [adult material][antiporn-dead] (💀)
1. Paypal is detrimental to **civil liberties**:
    1. PayPal has a history [littered](https://en.wikipedia.org/wiki/PayPal#Criticism) with power-abusing payment blockades that are often politically motivated to benefit right-wing agendas. E.g.:
        1. PayPal [froze accounts for left-wing journalists (Consortium News & MintPress)][frozemedia] apparently to hinder critics of Putin’s war in Ukraine.
        1. PayPal [blocked Iraq War resisters](https://en.wikipedia.org/wiki/PayPal#Criticism).
        1. PayPal [blocked Wikileaks][wikileaksBlocked-ia].
        1. PayPal [blocked][NemtsovBlocked-ia] an account intended to raise money for the distribution of Boris Nemtsov’s report "Putin. War", which details Russia's intervention in Ukraine.
        1. PayPal [blocks payments to VPN services][vpnBlocked].  This is a demonstration of nannying to micro-control people's spending. The bullying has a secondary impact: the user is refused access to a tool that secures their privacy.  Moreover, a Tor relay operator was [permanently barred by Paypal][permaBan] without warning and his assets seized for 180 days.
    1. PayPal forces users to execute non-free software, according to a comment on fsf.org's donation page (where they ironically accept PayPal).  This problem is considerable in light of how PayPal's partner (eBay) was [caught](http://web.archive.org/web/20200526092506/blog.nem.ec/2020/05/24/ebay-port-scanning) forcing its users to run javascript that does a port scan on the LAN, the results of which are sent back to eBay.
1. Paypal's greed **cheats people out of money** they're entitled to:
    1. PayPal [declined][fakeBugHunt] to pay a reward offered in its [Bug Bounty Program][bugBounty-ia] (⚠bad js) to a 17-year-old German student who had reported a cross-site scripting flaw on its site.
    1. PayPal is often [reported][moneygrab-dead] (💀) to simply take customers' money or deny them access as they [arbitrarily freeze](https://www.globes.co.il/news/article.aspx?did=1000998078) the accounts of [many people][manyFrozen] (⚠popup nags).
1. PayPal is detrimental to **consumer rights**:
    1. PayPal [prohibits][vpnProhibition] the use of VPNs.  Instead of simply blocking VPNs, PayPal sneakily allows VPN users to login in order to discover who uses a VPN, ultimately to [suspend][vpnSuspensions] accounts.  The suspension is generally timed so that money becomes trapped on the account.
    1. There is a maximum two accounts policy, and every account is single currency. So frequent international road warriers face discrimination and must pay more currency conversion fees.
    1. Hundreds of consumers complain about PayPal annually on the [Consumer Affairs website][complaints]. By 2016, there were over 1200 complaints.
    1. Staff becomes unreachable and website becomes inoperable when trying to [unfreeze][unfreezeIncompetence] accounts.
    1. PayPal algorithmically uses money laundering triggers that are so arbitrary that PayPal's customer service [don't know themselves][algoIncompetence-ia] why accounts get frozen.
    1. If you register again after previously closing your account, you [cannot][reregFail] add your credit card number because the number is permanently tied to previous closed account.
1. PayPal neglects to adhere to **banking regulations** by [claiming][notABank-ia] PayPal is not a bank.
1. PayPal [underreported][bankingpractices] their **carbon footprint** by 5,512%!
1. PayPal forces merchants to agree to facilitate [scraping][scrapesyou-dead] (💀) by PayPal to probe ToS.
1. PayPal subjects users to [swindles][ppswindle-ia].
1. PayPal created a “friends and family” mechanism which [exposes][friendsandfamily] customers to the threat of swindles and weasel-worded the terms of service to duck responsibility for defrauded customers.

# Notes

All links above are Cloudflare-free as of 2023-08-13. If you discover
a cloudflare link, please open an issue and we will deal with
it. Please also raise an issue to report misinfo, new info, or broken
links.

💀 ← Dead link indicator. Apologies for those. Three links cited
status from user “extinct” on Mastodon node “redroo.ml”. That node
went down. 

Help wanted: if anyone knows how to find copies of the missing posts
in federated timelines or can track down that user, it would be
helpful.

[//]: # (alternative to PayPal: Stripe API. This should be mentioned on a payment processor comparison page if we create one)
[//]: # (www.cnet.com uses the CF DNS which enables it to quickly proxy via CF,  We're not going to babysit which IP range is pointed to, so the link is via the Wayback Machine)
[//]: # (CloudFlare hosts related to links herein: blog.consumeraffairs.com, shop.economist.com, store.forbes.com)
[//]: # (to do: paypal breaches: https://carnegieendowment.org/specialprojects/protectingfinancialstability/timeline)

[frozemedia]: <https://www.massnews.com/paypal-seizes-alternative-media-sites-money-analysis>
[frozemedia-ia]: <http://web.archive.org/web/20230603175215/www.massnews.com/paypal-seizes-alternative-media-sites-money-analysis>

[bankingpractices]: <https://static1.squarespace.com/static/6281708e8ff18c23842b1d0b/t/6283204b3556a5125ce13b37/1652760661661/The+Carbon+Bankroll+Report+%285-17-2022%29.pdf>
[bankingpractices-ia]: <http://web.archive.org/web/20230523060733/static1.squarespace.com/static/6281708e8ff18c23842b1d0b/t/6283204b3556a5125ce13b37/1652760661661/The+Carbon+Bankroll+Report+(5-17-2022).pdf>

[antiporn-dead]: <https://redroo.ml/@extinct/108829838705907869>
[moneygrab-dead]: <https://redroo.ml/@extinct/108829434386993012>
[scrapesyou-dead]: <https://redroo.ml/@extinct/108829411496195433>

[ppswindle-dead]: <https://freeradical.zone/@Whiskerkenbrook/109468625739091649>
[ppswindle-ia]: <http://web.archive.org/web/20221210231104/freeradical.zone/@Whiskerkenbrook/109468625739091649>

[friendsandfamily]: <https://mastodon.social/@KatM/110203175529009174>
[friendsandfamily-ia]: <http://web.archive.org/web/20230415164846/mastodon.social/@KatM/110203175529009174>

[wikileaksBlocked-forbes]: <https://www.forbes.com/sites/andygreenberg/2010/12/07/visa-mastercard-move-to-choke-wikileaks>
[wikileaksBlocked-ia]: <http://web.archive.org/web/20230524124146/www.forbes.com/sites/andygreenberg/2010/12/07/visa-mastercard-move-to-choke-wikileaks>

[NemtsovBlocked-cf]: <https://www.economist.com/europe/2015/05/13/boris-nemtsovs-parting-shot>
[NemtsovBlocked-ia]: <http://web.archive.org/web/20230726004255/www.economist.com/europe/2015/05/13/boris-nemtsovs-parting-shot>

[permaBan]: <https://lists.torproject.org/pipermail/tor-relays/2021-March/019408.html>
[permaBan-ia]: <http://web.archive.org/web/20221021162308/https://lists.torproject.org/pipermail/tor-relays/2021-March/019408.html>

[vpnBlocked]: <https://www.finder.com.au/paypal-starts-blocking-vpn-payments-what-should-you-do>
[vpnBlocked-ia]: <http://web.archive.org/web/20220830085453/www.finder.com.au/paypal-starts-blocking-vpn-payments-what-should-you-do>

[manyFrozen]: <https://mywifequitherjob.com/why-paypal-freezes-or-limits-accounts-and-how-to-prevent-this-from-happening-to-you>
[manyFrozen-ia]: <http://web.archive.org/web/20230601163130/mywifequitherjob.com/why-paypal-freezes-or-limits-accounts-and-how-to-prevent-this-from-happening-to-you>

[fakeBugHunt]: <https://www.pcworld.com/article/2039940/paypal-denies-teenager-reward-for-finding-website-bug.html>
[fakeBugHunt]: <http://web.archive.org/web/20230404180740/www.pcworld.com/article/452079/paypal-denies-teenager-reward-for-finding-website-bug.html>

[bugBounty-cf]: <https://hackerone.com/paypal>
[bugBounty-ia]: <https://web.archive.org/web/20230618164401/hackerone.com/paypal>

[vpnProhibition]: <https://www.paypal-community.com/t5/PayPal-Basics/paypal-and-VPN-problem/td-p/1353216>
[vpnProhibition-ia]: <http://web.archive.org/web/20230813094012/www.paypal-community.com/t5/Managing-Account-Archives/paypal-and-VPN-problem/td-p/1353216>

[vpnSuspensions]: <https://old.reddit.com/r/VPN/comments/63t8kg/is_it_against_paypals_terms_to_use_a_vpn>
[vpnSuspensions-ia]: <http://web.archive.org/web/20230607031444/old.reddit.com/r/VPN/comments/63t8kg/is_it_against_paypals_terms_to_use_a_vpn>

[complaints]: <https://www.consumeraffairs.com/online/paypal_02.html>
[complaints-ia]: <http://web.archive.org/web/20230404143726/www.consumeraffairs.com/online/paypal-us.html>

[unfreezeIncompetence]: <https://mirasee.com/blog/paypal>
[unfreezeIncompetence-ia]: <http://web.archive.org/web/20221005084754/mirasee.com/blog/paypal>

[algoIncompetence-cf]: <https://www.computerweekly.com/blog/Cliff-Sarans-Enterprise-blog/PayPal-money-laundering-nonsense>
[algoIncompetence-ia]: <http://web.archive.org/web/20221004100615/www.computerweekly.com/blog/Cliff-Sarans-Enterprise-blog/PayPal-money-laundering-nonsense>

[reregFail]: <https://www.paypal-community.com/t5/My-Money/HOW-TO-DE-LINK-CARD-FROM-CLOSED-ACCOUNT-AND-LINK-IT-TO-NEW-ONE/td-p/1641244>
[reregFail-ia]: <http://web.archive.org/web/20220914162909/www.paypal-community.com/t5/My-Money-Archives/HOW-TO-DE-LINK-CARD-FROM-CLOSED-ACCOUNT-AND-LINK-IT-TO-NEW-ONE/td-p/1641244>

[notABank-ia]: <http://web.archive.org/web/20221013001238/www.cnet.com/tech/tech-industry/feds-paypal-not-a-bank>

