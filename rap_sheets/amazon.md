[//]: # (to do: news sources could show a tracker count: https://source.small-tech.org/better/site/-/tree/master/content/sites/alexa-top-500-news and perhaps use these svcs when the count is high: https://gitlab.torproject.org/legacy/trac/-/wikis/org/doc/ListOfServicesBlockingTor#ad-hoc-solutions-for-accessing-blocked-content-on-tor)

# Amazon

Amazon is the root of abuses of privacy, freedom, human rights, civil liberties, consumer rights, and the environment:

1. Amazon mounts mutlifaceted attacks on **privacy**
    1. Amazon is making an astronomical investment in facial recognition to exploit a market worth [$8 billion][facial8bn] which will destroy physical travel privacy worldwide.  Amazon's innaccurate technology [erroneously matched][facialAccuracyBad] 100 US and UK politicians to criminals.  Amazon also developed the technology by [unlawfully](https://web.archive.org/web/20201226111112/https://www.cnet.com/news/amazon-google-and-microsoft-sued-over-photos-in-facial-recognition-database) using people's images without consent to train facial recognition products.
    1. Amazon deploys Ring, Alexa, and Sidewalk to surveil neighborhoods and surveil the inside of homes.
        1. Amazon kept everyone's Alexa recordings and transcripts [indefinitely][alexaForever], until public backlash coerced them to [offer a delete feature](https://web.archive.org/web/20210115031227/https://www.cnet.com/news/alexa-can-now-immediately-delete-your-voice-recordings) in 2020 4th quarter-- a "feature" that a socially responsible company would have included at inception.  The deletions are not enabled by default (users must login to the web portal and tweak their settings manually, at which point Amazon tries to discourage users by saying Alexa won't be as smart without history).
        1. Sidewalk creates a private network by [exploiting the "smart" home devices][sidewalk] in a way that data can become vulnerable to inappropriate access.
        1. The LAPD has been [caught][ring-police] accepting free and discounted Amazon Ring cameras, which violates laws prohibiting police from accepting gifts.
    1. Amazon was caught equipping their delivery (surveillance) trucks to [grab data][surveillancecams] wherever they go from all who come in contact with it, faces, wifi IDs, bluetooth signatures, etc, (not just from drivers as claimed).
    1. Amazon’s Echo and the smart TVs [monitor everything you do][snoopOnAll] -- [even if you disable ad reporting][snoopWhenOff].  You used to simply watch TV, but now your TV is watching you.  (Note the research paper is not open to the public because ACM locks their digital library in the exclusive walled-garden of CloudFlare)
    1. Amazon's CTO (Werner Vogels) [admitted][snooping-confession] that Amazon tries to "collect as much information as possible" and a former Amazon executive [said][snooping-confession] Amazon "happens to sell products, but they are a data company".
    1. Amazon [paid][CCPA195k] $195k to fight privacy in California by lobbying [against the CCPA][CCPALobby].
    1. Amazon [supported](http://www.allcom.se/it-nyheter/apple-makes-a-stand-against-cisa-cybersecurity-bill) the Cybersecurity Information Sharing Act ([CISA](https://truthinmedia.com/cisa-supporters-under-threat-from-anonymous-splinter-group/)), a recycled version of CISPA, which passed after CISPA was voted down.  CISA facilitates corporation-government information oversharing in a way that circumvents protections under the 4th amendment of the US Constitution. Amazon appeared on both sides of this, as it wanted the public to believe that Amazon [opposed](https://www.infosecurity-magazine.com/news/tech-giants-oppose-us-threat-intel) CISA. (*citation needed-- the original allcom.se link is dead and allcom.se blocks archival*)
    1. *War on cash* is war on privacy: Amazon's grocery stores [do not accept cash][WarOnCashGrocery]. They impose the same surveillance as ordering online from Amazon.  Cashless shops discriminate against the [6.5% of the US population][warOnCash] that does not have a bank account.
    1. Amazon spent $30 million and ranked in the top 5 promoters of Facebook ads in 2012 (thus substantially feeding a privacy abuser).
    1. Facebook and Amazon made [a secret deal][FBshare] to give Amazon access to Facebook's data about users.
    1. The Kindle informs Amazon when the user reads books that didn't come from Amazon. It also tells Amazon which pages each user reads.  This is perhaps one of the many reasons Richard Stallman calls refers to the Kindle as the "*Amazon Swindle*".
    1. Amazon has [been caught][DRMabuse] deleting accounts and abusing the power of DRM to destroy users' entire library of books without explanation, if they suspect a Kindle user is also using a rival device.  It's common enough and due to Amazon's refusal to explain the problem, some people [suspect it's a bug][DRMdeception].
    1. Amazon distributes NRAtv which promotes a privacy-hostile political party and the resulting policies.  Also sells the Trump line of suits in their webshop.
    1. Sensitive data for 100 million people banking at Capital One was [leaked][caponeleak] by an Amazon worker.  Amazon [refuses blame][caponeNoBlame] for it.  Capital One was [fined $80M](https://web.archive.org/web/20200925231658/https://www.zdnet.com/article/capital-one-fined-80-million-for-2019-hack) for failing to do risk assessment.
    1. Amazon was [fined][gdprviolation] €746 million GDPR violations.
1. Amazon is responsible for **human rights** and **civil liberties** abuses
    1. Amazon [supplies][empBoycott] [unlawfully developed](https://web.archive.org/web/20201226111112/https://www.cnet.com/news/amazon-google-and-microsoft-sued-over-photos-in-facial-recognition-database/) facial recognition to law enforcement who use it to abuse civil liberties, despite [protest](https://thehill.com/business-a-lobbying/393583-amazon-employees-protest-sale-of-facial-recognition-tech-to-law) by Amazon employees, 40 civil rights organizations, and [150,000 petitioners](https://web.archive.org/web/20201020003009/https://www.zdnet.com/article/now-amazon-employees-rebel-end-police-facial-recognition-contracts-ice-support).
    1. Amazon [supplies][ICEAWS] AWS [to ICE (bad link)][ICEspending] and Palantir, a database firm that exploits social media to [facilitate][ICEfacilitate] ICE and CBP to enforce Trump's inhumane *zero tolerance* immigration policy that entails child-parent separation.  Palantir was also co-founded by a notorious xenophobic and billionaire backer of Donald Trump: Peter Thiel.  Peter Thiel founded Palantir to [help ICE][thielPatriotism] deploy algorithms that find people to deport.  Peter Thiel called Google "unpatriotic" for "[not embracing opportunities][thielPatriotism] to work with federal agencies" thinking that Google appeased employees who opposed inhumane treatment of immigrants (he was unaware that Google's announcement and action differed).
    1. Amazon supports Breitbart (the right-wing extremist site) by [advertising][breitbartSupport-cache] there.
    1. Amazon uses FedEx (an NRA-supporting ALEC member who feeds republican warchests via ALEC and NRA [republican policy is xenophobic and detrimental to gun control and individual privacy]).
    1. Amazon in Germany [hired][naziGuards] "security" guards from a company of Nazi sympathizers to intimidate and repress foreign workers. Reporters came to cover this, and the guards tried to arrest them and take their cameras. (2013)
    1. Amazon willfully sells a product [it knows is being used for suicide][suicide]. Amazon’s algorithms target potential suicide victims with ads for other products that will aid in the suicide.
    1. Amazon sells [racially biased][facialbias] facial recognition technology.
    1. Amazon [pollutes disproportionately][racialpollution-cache] in communities of color.
1. Amazon is detrimental to **consumer rights**
    1. Amazon distributes ebooks in a way that [strips][ebookTradeoff] users of many freedoms.
    1. The Amazon Kindle has a back door that can erase books.  Amazon was [caught][kindleErase] remotely erasing thousands of copies of 1984.
    1. Amazon rents textbooks to students with a requirement not to take them [across state lines](http://web.archive.org/web/20201112040809/https://www.insidehighered.com/news/2013/08/16/amazon-restricts-students-bringing-certain-textbook-rentals-across-state-lines).
    1. Amazon was [caught][personalizedpricing] using *personalized pricing* (charged different prices to different people for DVDs)
1. Amazon is notorious for **fighting unions** and **mistreating employees** despite its wealth and growth.
    1. Amazon runs an extreme sweatshop that diminishes quality of life.  The consequential mental health crisis is [evidenced][callsTo911] by 189 calls from Amazon warehouses to 911 in five years.
    1. Workers [pee in bottles][piss].
    1. Amazon has [chickenized reverse-centaurs][chickenization].
    1. Amazon has put its drivers under AI surveillance and [forced an ultimatum][surveillanceConsent]: sign the agreement or be sacked.
    1. Amazon uses automation that [increases injury rates][roboinjury].
    1. Amazon [enabled covid19-infected workers][covid] to work, which caused several outbreaks in warehouses, then Amazon retaliated against whistle blowers.
    1. Amazon coerced a city to [alter traffic light timing][trafficManipulation] so union organizers couldn't use a red traffic light to reach workers.
    1. Amazon was [fined][tiptheft] $61.7 million for keeping drivers' tips.
    1. Amazon drug tests its employees, thus intruding on their privacy outside the workplace and also harming their healthcare.
    1. [oppressive and callous attitude][oppression] toward staff.
    1. [55-hour work weeks][work55hrs]
    1. 90,000+ warehouse employees treated like cattle ([7 examples][cattleStaff])
    1. Amazon [exploits workers and fights unions][tenReasons]. Amazon [launched][antiunion] an anti-union [propaganda](https://laserdisc.party/@radicalrobit/105701585218038720) website to hinder formation of unions in Alabama.
    1. Amazon employees [report years of racism][racism], such as [underpayment, denied promotions, and sexual harassment][racismCase] in [numerous][antiblack] cases.
    1. Amazon [discriminates against muslim people][antimuslim]
1. Amazon proliferates **censorship**
    1. Amazon has [partnered with the MPAA](https://web.archive.org/web/20201126140203/https://torrentfreak.com/inside-the-mpaa-netflix-amazon-global-anti-piracy-alliance-170918) to campaign for repression of sharing on the net.
    1. Amazon cut off service to Wikileaks, claiming that [whistle-blowing violates its terms of service][antiwhistle].
    1. Amazon Prime Video is strictly region-locked, denying content to people in regions that they discriminate against. Users who attempt to bypass the censorship, for example to watch an American film from non-US country via VPN to the US will be blocked immediately.
    1. Amazon [censored][suicide] product reviews that warn that a product Amazon sells is being used for suicide.
1. Amazon is detrimental to the **environment**
    1. Amazon [powers][tenReasons] 50% of their servers with unclean energy.
    1. Amazon's excessive packaging [destroys][pkgExcess] 1 billion trees annually.  ([examples][pkgEg])
    1. Amazon [retaliates][antiactivist] against employees who seek climate action.
    1. Amazon works for BP and Shell to deliver a [machine learning service][AIfossil] to discover locations to drill for oil and gas.
    1. Amazon has [been caught][climateDenial] financing climate deniers.
    1. Amazon's extremely low prices encourage reckless consumption that's environmentally harmful.  Amazon-branded devices in particular (Kindle, Alexa, Tablets) become dirt-cheap on sitewide sale days such as Black Friday.
    1. Amazon fully exploits [The Tyranny of Convenience](https://www.nytimes.com/2018/02/16/opinion/sunday/tyranny-convenience.html), so even if prices weren't competitive the convenience they bring encourages wasteful consumption.
    1. Amazon is [destroying millions of items][wastes-stock] of
       unsold stock every year, products that are often new and
       unused, ITV News can reveal.  (That article covers the UK, but
       an insider tells me it's happening in the US too)
    1. Amazon’s [banking practices][bankingpractices] have a higher carbon emissions than the actual energy Amazon purchases to run their warehouses & fulfill orders.
1. Amazon is **anti-consumer** and anti-competitive
    1. Amazon [sold diapers at a loss](https://web.archive.org/web/20201231204553/https://www.currentaffairs.org/2020/12/how-amazon-destroys-the-intellectual-justifications-for-capitalism/) to drive Quidsi out of business.
    1. Amazon spent $4.38 million to [lobby against antitrust law][antitrustLobby]
    1. Amazon creates an elitist class system whereby sales of some goods are limited to Prime members only. Non-prime members are denied service.  Amazon's market domination has reached a point where consumers often cannot find what they need outside of Amazon's store.
    1. Amazon makes [guinea pigs of customers][guinea-pigs] to develop highly experimental products. Who pays for that? The customer, who doesn't get a refund when Amazon pulls the plug and the Internet connected device self destructs after the warranty ends.
    1. When Amazon [throws away millions of items][wastes-stock] instead of selling them to an overstock specialist or donating to charity, it's not just bad for the environment, it's also anti-competitive likely in breach of anti-trust laws.
    1. Amazon was [caught lying to Congress][liedtocongress] about its use of seller data.
    1. Amazon [used minority groups to deceive Congress][sneakyTricks-cache] in an attempt to criticize the anti-trust bill “American Choice and Innovation Online Act”, S.2992, despite mistreating its own black and muslim employees.
    
See also Richard Stallman's [Amazon RAP sheet][RMS], which we will one day merge into the above.

[//]: # (the following links are bad or non-archivable using the wayback machine.  we need to replace these articles with a different source)

[breitbartSupport-cache]: <http://web.archive.org/web/www.valuewalk.com/2017/02/337k-amazon-ceo-jeff-bezos-stop-advertising-breitbart>
  "Bad link! Native site is CloudFlare-jailed so we must avoid it, but it's also anti-wayback-machine"

[antitrustLobby]: <https://www.bloomberg.com/news/articles/2020-07-21/amazon-sets-new-lobbying-record-as-tech-antitrust-scrutiny-grows>
  "Fragile article! Anti-bot mechanism blocks archival, so if this page goes offline we're fucked."

[antitrustLobby-cache]: <https://web.archive.org/web/www.bloomberg.com/news/articles/2020-07-21/amazon-sets-new-lobbying-record-as-tech-antitrust-scrutiny-grows>
  "Broken due to anti-bot mechanism!"

[ICEspending]: <https://www.usaspending.gov/#/award/62522780> "Data was removed before it could be archived."
[ICEspending-cache]: <https://web.archive.org/web/20210204204928if_/https://www.usaspending.gov/award/62522780> "Data was removed before it could be archived."

[//]: # (the following links tagged "-cache" are working cached versions, preserved here in case a link on the RAP sheet does bad)

[facial8bn]: <https://www.forbes.com/sites/korihale/2020/06/15/amazon-microsoft--ibm-slightly-social-distancing-from-the-8-billion-facial-recognition-market>
[facial8bn-cache]: <https://web.archive.org/web/20210113184348/www.forbes.com/sites/korihale/2020/06/15/amazon-microsoft--ibm-slightly-social-distancing-from-the-8-billion-facial-recognition-market>

[facialAccuracyBad]: <https://www.independent.co.uk/life-style/gadgets-and-tech/news/amazon-facial-recognition-false-positives-recognition-congress-criminals-a9536351.html>
[facialAccuracyBad-cache]: <https://web.archive.org/web/20210107041413/www.independent.co.uk/life-style/gadgets-and-tech/news/amazon-facial-recognition-false-positives-recognition-congress-criminals-a9536351.html>

[alexaForever]: <https://arstechnica.com/tech-policy/2019/07/amazon-confirms-it-keeps-your-alexa-recordings-basically-forever>
[alexaForever-cache]: <https://web.archive.org/web/20210112204936/arstechnica.com/tech-policy/2019/07/amazon-confirms-it-keeps-your-alexa-recordings-basically-forever>

[snoopOnAll]: <https://www.theguardian.com/technology/shortcuts/2014/nov/09/amazon-echo-smart-tv-watching-listening-surveillance>
[snoopOnAll-cache]: <https://web.archive.org/web/20201120043824/www.theguardian.com/technology/shortcuts/2014/nov/09/amazon-echo-smart-tv-watching-listening-surveillance>

[snoopWhenOff]: <https://blog.acolyer.org/2020/02/10/watching-you-watch>
[snoopWhenOff-cache]: <https://web.archive.org/web/20210112164231/blog.acolyer.org/2020/02/10/watching-you-watch>

[CCPA195k]: <http://cal-access.sos.ca.gov/Campaign/Committees/Detail.aspx?id=1401518&session=2017&view=received>
[CCPA195k-cache]: <https://web.archive.org/web/20200611155900/http://cal-access.sos.ca.gov/Campaign/Committees/Detail.aspx?id=1401518&view=received&session=2017>

[CCPALobby]: <https://arstechnica.com/tech-policy/2018/04/facebook-donated-200000-to-kill-a-privacy-law-but-now-its-backtracking>
[CCPALobby-cache]: <https://web.archive.org/web/20210127212444/arstechnica.com/tech-policy/2018/04/facebook-donated-200000-to-kill-a-privacy-law-but-now-its-backtracking>

[WarOnCashGrocery]: <http://motherboard.vice.com/read/amazon-go-isnt-trying-to-kill-cashier-jobs-its-after-something-bigger>
[WarOnCashGrocery-cache]: <https://web.archive.org/web/20201111221244/www.vice.com/en/article/nz7k8x/amazon-go-isnt-trying-to-kill-cashier-jobs-its-after-something-bigger>

[warOnCash]: <https://www.fastcompany.com/90389594/aclu-cash-free-retail-amazon-sweetgreen-privacy>
[warOnCash-cache]: <https://web.archive.org/web/20210111152750/www.fastcompany.com/90389594/aclu-cash-free-retail-amazon-sweetgreen-privacy>

[FBshare]: <https://gizmodo.com/amazon-and-facebook-reportedly-had-a-secret-data-sharin-1831192148>
[FBshare-cache]: <https://web.archive.org/web/20201220065844/gizmodo.com/amazon-and-facebook-reportedly-had-a-secret-data-sharin-1831192148>

[caponeleak]: <https://www.forbes.com/sites/rachelsandler/2019/07/29/capital-one-says-hacker-breached-accounts-of-100-million-people-ex-amazon-employee-arrested>
[caponeleak-cache]: <https://web.archive.org/web/20200927211213/www.forbes.com/sites/rachelsandler/2019/07/29/capital-one-says-hacker-breached-accounts-of-100-million-people-ex-amazon-employee-arrested>

[caponeNoBlame]: <https://www.newsweek.com/amazon-capital-one-hack-data-leak-breach-paige-thompson-cybercrime-1451665>
[caponeNoBlame-cache]: <https://web.archive.org/web/20200618091312/www.newsweek.com/amazon-capital-one-hack-data-leak-breach-paige-thompson-cybercrime-1451665>

[empBoycott]: <https://www.seattletimes.com/business/amazon-employees-demand-company-cut-ties-with-ice>
[empBoycott-cache]: <https://web.archive.org/web/20210101231951/www.seattletimes.com/business/amazon-employees-demand-company-cut-ties-with-ice>

[ICEAWS]: <https://thehill.com/business-a-lobbying/393583-amazon-employees-protest-sale-of-facial-recognition-tech-to-law>
[ICEAWS-cache]: <https://web.archive.org/web/20210119235238/thehill.com/business-a-lobbying/393583-amazon-employees-protest-sale-of-facial-recognition-tech-to-law>

[ICEfacilitate]: <https://www.govtech.com/biz/Documents-Reveal-ICE-Used-Palantir-for-Deportations.html>
[ICEfacilitate-cache]: <https://web.archive.org/web/20201112032136/www.govtech.com/biz/Documents-Reveal-ICE-Used-Palantir-for-Deportations.html>

[thielPatriotism]: <https://www.businessinsider.com/us-customs-border-protection-testing-google-cloud-anthos-2019-8>
[thielPatriotism-cache]: <https://web.archive.org/web/20210213222606/www.businessinsider.com/us-customs-border-protection-testing-google-cloud-anthos-2019-8?r=US&IR=T>

[naziGuards]: <https://www.independent.co.uk/news/world/europe/amazon-used-neo-nazi-guards-to-keep-immigrant-workforce-under-control-in-germany-8495843.html>
[naziGuards-cache]: <https://web.archive.org/web/20201119144746/https://www.independent.co.uk/news/world/europe/amazon-used-neo-nazi-guards-keep-immigrant-workforce-under-control-germany-8495843.html>

[ebookTradeoff]: <http://gnu.org/philosophy/the-danger-of-ebooks.html>
[ebookTradeoff-cache]: <http://web.archive.org/web/20201112013840/www.gnu.org/philosophy/the-danger-of-ebooks.html>

[kindleErase]: <http://pogue.blogs.nytimes.com/2009/07/17/some-e-books-are-more-equal-than-others>
[kindleErase-cache]: <http://web.archive.org/web/20201116231255/pogue.blogs.nytimes.com/2009/07/17/some-e-books-are-more-equal-than-others>

[callsTo911]: <https://gizmodo.com/report-amazon-warehouses-called-911-for-mental-health-1833220938>
[callsTo911-cache]: <https://web.archive.org/web/20210115073502/gizmodo.com/report-amazon-warehouses-called-911-for-mental-health-1833220938>

[piss]: https://www.theverge.com/2021/3/25/22350337/amazon-peeing-in-bottles-workers-exploitation-twitter-response-evidence#comments
[piss-cache]: https://web.archive.org/web/20210326151259/www.theverge.com/2021/3/25/22350337/amazon-peeing-in-bottles-workers-exploitation-twitter-response-evidence

[chickenization]: https://pluralistic.net/2021/03/19/the-shakedown/#weird-flex
[chickenization-cache]: https://web.archive.org/web/20210319201637/https://pluralistic.net/2021/03/19/the-shakedown/#weird-flex

[surveillanceConsent]: https://www.vice.com/en/article/dy8n3j/amazon-delivery-drivers-forced-to-sign-biometric-consent-form-or-lose-job
[surveillanceConsent-cache]: https://web.archive.org/web/20210323164942/www.vice.com/en/article/dy8n3j/amazon-delivery-drivers-forced-to-sign-biometric-consent-form-or-lose-job

[tiptheft]: https://slate.com/news-and-politics/2021/02/amazon-ftc-pay-flex-drivers-stolen-tips.html
[tiptheft-cache]: https://web.archive.org/web/20210315082128/slate.com/news-and-politics/2021/02/amazon-ftc-pay-flex-drivers-stolen-tips.html

[roboinjury]: https://www.ft.com/content/087fce16-3924-4348-8390-235b435c53b2
[roboinjury-cache]: https://web.archive.org/web/20210309144309/www.ft.com/content/087fce16-3924-4348-8390-235b435c53b2

[covid]: https://pluralistic.net/2020/04/03/socially-useless-parasite/#christian-smalls
[covid-cache]:https://web.archive.org/web/20210308142716/https://pluralistic.net/2020/04/03/socially-useless-parasite

[trafficManipulation]: https://www.theverge.com/2021/2/17/22287191/amazon-alabama-warehouse-union-traffic-light-change-bessemer
[trafficManipulation-cache]: <https://web.archive.org/web/20210312181909/www.theverge.com/2021/2/17/22287191/amazon-alabama-warehouse-union-traffic-light-change-bessemer> "antifeature: cookie nag popup"

[oppression]: <https://www.independent.co.uk/news/business/news/amazon-devastating-expose-accuses-internet-retailer-of-oppressive-and-callous-attitude-to-staff-10458159.html>
[oppression-cache]: <https://web.archive.org/web/20201111174127/www.independent.co.uk/news/business/news/amazon-devastating-expose-accuses-internet-retailer-oppressive-and-callous-attitude-staff-10458159.html>

[work55hrs]: <https://www.independent.co.uk/news/uk/home-news/amazon-workers-working-hours-weeks-conditions-targets-online-shopping-delivery-a8079111.html>
[work55hrs-cache]: <https://web.archive.org/web/20210125030350/www.independent.co.uk/news/uk/home-news/amazon-workers-working-hours-weeks-conditions-targets-online-shopping-delivery-a8079111.html>

[cattleStaff]: <https://www.pastemagazine.com/articles/2017/12/7-examples-how-amazon-treats-their-90000-warehouse.html>
[cattleStaff-cache]: <https://web.archive.org/web/20201112024907/www.pastemagazine.com/politics/amazon/7-examples-how-amazon-treats-their-90000-warehouse>

[antiwhistle]: <http://www.theguardian.com/media/blog/2010/dec/03/wikileaks-knocked-off-net-dns-everydns>
[antiwhistle-cache]: <http://web.archive.org/web/20201109013725/www.theguardian.com/media/blog/2010/dec/03/wikileaks-knocked-off-net-dns-everydns>

[tenReasons]: <https://www.greenamerica.org/blog/10-reasons-not-shop-amazon-prime>
[tenReasons-cache]: <https://web.archive.org/web/20201127173709/www.greenamerica.org/blog/10-reasons-not-shop-amazon-prime>

[pkgExcess]: <https://www.forbes.com/sites/jonbird1/2018/07/29/what-a-waste-online-retails-big-packaging-problem>
[pkgExcess-cache]: <https://web.archive.org/web/20210128192439/www.forbes.com/sites/jonbird1/2018/07/29/what-a-waste-online-retails-big-packaging-problem>

[pkgEg]: <https://www.buzzfeed.com/morenikeadebayo/amazon-packaging-needs-to-chill-the-fuck-out>
[pkgEg-cache]: <https://web.archive.org/web/20201108143901/www.buzzfeed.com/morenikeadebayo/amazon-packaging-needs-to-chill-the-fuck-out>

[antiactivist]: <https://www.tbray.org/ongoing/When/202x/2020/04/29/Leaving-Amazon>
[antiactivist-cache]: <https://web.archive.org/web/20210105043248/www.tbray.org/ongoing/When/202x/2020/04/29/Leaving-Amazon>

[AIfossil]: <https://yewtu.be/watch?v=v3n8txX3144>
[AIfossil-cache]: <https://web.archive.org/web/20210205031444/yewtu.be/watch?v=v3n8txX3144>

[climateDenial]: <https://www.theguardian.com/environment/2019/oct/11/google-contributions-climate-change-deniers>
[climateDenial-cache]: <https://web.archive.org/web/20210130235719/www.theguardian.com/environment/2019/oct/11/google-contributions-climate-change-deniers>

[RMS]: <https://stallman.org/amazon.html>
[RMS-cache]: <https://web.archive.org/web/20210115131155/stallman.org/amazon.html>

[DRMdeception]: <https://ebookevangelist.com/2016/04/22/did-your-fire-tablet-just-deregister-itself/>
[DRMdeception-cache]: <https://web.archive.org/web/20200922013148/ebookevangelist.com/2016/04/22/did-your-fire-tablet-just-deregister-itself/>

[DRMabuse]: <https://www.theguardian.com/money/2012/oct/22/amazon-wipes-customers-kindle-deletes-account>
[DRMabuse-cache]: <https://web.archive.org/web/20201112031605/www.theguardian.com/money/2012/oct/22/amazon-wipes-customers-kindle-deletes-account>

[antiunion]: <https://www.vice.com/en/article/5dpkad/amazon-launches-anti-union-website-to-derail-alabama-union-drive>
[antiunion-cache]: <https://web.archive.org/web/20210206003806/www.vice.com/en/article/5dpkad/amazon-launches-anti-union-website-to-derail-alabama-union-drive>

[personalizedpricing]: <https://www.consumerreports.org/car-insurance/why-you-may-be-paying-too-much-for-your-car-insurance>
[personalizedpricing-cache]: <https://web.archive.org/web/20210129175046/www.consumerreports.org/car-insurance/why-you-may-be-paying-too-much-for-your-car-insurance>

[racism]: <https://www.vox.com/recode/2021/2/26/22297554/bias-disrespect-and-demotions-black-employees-say-amazon-has-a-race-problem>
[racism-cache]: <https://web.archive.org/web/20210226153714/www.vox.com/recode/2021/2/26/22297554/bias-disrespect-and-demotions-black-employees-say-amazon-has-a-race-problem>

[racismCase]: <https://arstechnica.com/tech-policy/2021/03/aws-director-sues-amazon-alleging-systemic-racism-in-corporate-office>
[racismCase-cache]: <https://web.archive.org/web/20210302214604/arstechnica.com/tech-policy/2021/03/aws-director-sues-amazon-alleging-systemic-racism-in-corporate-office>

[antiblack]: https://www.vox.com/recode/2021/2/26/22297554/amazon-race-black-diversity-inclusion
[antiblack-cache]: http://web.archive.org/web/20220607041245/www.vox.com/recode/2021/2/26/22297554/amazon-race-black-diversity-inclusion

[antimuslim]: https://www.nytimes.com/2019/05/08/technology/amazon-muslim-workers-complaint.html
[antimuslim-cache]: http://web.archive.org/web/20220603222420/www.nytimes.com/2019/05/08/technology/amazon-muslim-workers-complaint.html

[facialbias]: https://www.theverge.com/2019/1/25/18197137/amazon-rekognition-facial-recognition-bias-race-gender
[facialbias-cache]: http://web.archive.org/web/20220603222314/www.theverge.com/2019/1/25/18197137/amazon-rekognition-facial-recognition-bias-race-gender

[racialpollution]: https://www.consumerreports.org/corporate-accountability/when-amazon-expands-these-communities-pay-the-price-a2554249208/ "tor-hostile 403"
[racialpollution-cache]: http://web.archive.org/web/20220603222308/www.consumerreports.org/corporate-accountability/when-amazon-expands-these-communities-pay-the-price-a2554249208/

[guinea-pigs]: <https://www.nytimes.com/2021/06/16/technology/personaltech/buyers-of-amazon-devices-are-guinea-pigs-thats-a-problem.html?smid=tw-share>
[guinea-pigs-cache]: <https://www.nytimes.com/2021/06/16/technology/personaltech/buyers-of-amazon-devices-are-guinea-pigs-thats-a-problem.html?smid=tw-share>

[wastes-stock]: <https://www.itv.com/news/2021-06-21/amazon-destroying-millions-of-items-of-unsold-stock-in-one-of-its-uk-warehouses-every-year-itv-news-investigation-finds>
[wastes-stock-cache]: <https://web.archive.org/web/20210621101104/https://www.itv.com/news/2021-06-21/amazon-destroying-millions-of-items-of-unsold-stock-in-one-of-its-uk-warehouses-every-year-itv-news-investigation-finds>

[sidewalk]: <https://www.commondreams.org/views/2021/06/20/aclu-alarm-amazon-activates-sidewalk-surveillance-system>
[sidewalk-cache]: <https://web.archive.org/web/20210621074342/https://www.commondreams.org/views/2021/06/20/aclu-alarm-amazon-activates-sidewalk-surveillance-system>

[snooping-confession]: <https://www.wired.com/story/amazon-tracking-how-to-stop-it>
[snooping-confession-cache]: <https://web.archive.org/web/20210623152424/www.wired.com/story/amazon-tracking-how-to-stop-it>

[ring-police]: <https://www.eff.org/deeplinks/2021/06/emails-show-amazon-rings-hold-lapd-through-camera-giveaways>
[ring-police-cache]: <https://web.archive.org/web/20210623025357/https://www.eff.org/deeplinks/2021/06/emails-show-amazon-rings-hold-lapd-through-camera-giveaways>

[gdprviolation]: <https://www.cnet.com/news/amazon-hit-with-record-888m-fine-over-gdpr-violations>
[gdprviolation-cache]: <http://web.archive.org/web/20210731033405/www.cnet.com/news/amazon-hit-with-record-888m-fine-over-gdpr-violations>

[suicide]: <https://www.nytimes.com/2022/02/04/technology/amazon-suicide-poison-preservative.html>
[suicide-cache]: <https://web.archive.org/web/20220205064526/www.nytimes.com/2022/02/04/technology/amazon-suicide-poison-preservative.html>

[liedtocongress]: https://arstechnica.com/tech-policy/2022/03/us-lawmakers-seek-criminal-probe-of-amazon-for-lying-about-use-of-seller-data/
[liedtocongress-cache]: http://web.archive.org/web/20220309180622/arstechnica.com/tech-policy/2022/03/us-lawmakers-seek-criminal-probe-of-amazon-for-lying-about-use-of-seller-data/

[bankingpractices]: https://static1.squarespace.com/static/6281708e8ff18c23842b1d0b/t/6283204b3556a5125ce13b37/1652760661661/The+Carbon+Bankroll+Report+%285-17-2022%29.pdf

[surveillancecams]: https://unredactedmagazine.com/issues/002.pdf?page=17

[sneakyTricks-cache]: https://web.archive.org/web/20220607193307/www.politico.com/news/2022/06/03/amazon-urges-consultant-to-push-message-from-minority-groups-00037169

